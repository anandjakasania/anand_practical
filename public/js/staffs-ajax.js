
$.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

$('body').on('click', '.update_btn', function () {
        if($("#staff_create_frm").valid()){
            var form_data = new FormData($("#staff_create_frm")[0]);
            $.ajax({
                url: '/staff_update',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    manageData();
                },
                error: function(data)
                {
                    console.log('error',data);
                }
            });

        }
      });

$('body').on('click', '.submit_btn', function () {
        if($("#staff_create_frm").valid()){
            var file_data = $('#profile_pic_id').prop('files')[0];
            var form_data = new FormData($("#staff_create_frm")[0]);
            /*form_data.append('file', file_data);*/
            $.ajax({
                url: create_url,
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    manageData();
                },
                error: function(data)
                {
                    console.log('error',data);
                }
            });

        }
      });
var page = 1;
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;
manageData();

/* manage data list */

function manageData() {
    $('.staff_create').css('display','none');
    $('.staff_record').css('display','block');

    $.ajax({
        dataType: 'json',
        url: url
        /*data: {page:page}*/
    }).done(function(data){
    	manageRow(data);
        is_ajax_fire = 1;
    });

}


/* Add new Post table row */

function manageRow(data) {

	var	rows = '';
    var i=1;
    
    var staffs=data.staffs;
	$.each( staffs, function( key, value ) {

	  	rows = rows + '<tr>';

	  	rows = rows + '<td>'+i+'</td>';

	  	rows = rows + '<td><input type="checkbox" class="staff_sel_cls" name="staff_sel[]" id="staff_sel" value="'+value.id+'"></td>';

        rows = rows + '<td>'+value.staff_name+'</td>';
        rows = rows + '<td>'+value.contact_number+'</td>';
        rows = rows + '<td>'+value.category.category_name+'</td>';
        rows = rows + '<td><img src="images/'+value.profile_pic+'" width="100" height="100"></td>';
	  	rows = rows + '<td data-id="'+value.id+'">';

        rows = rows + '<button data-toggle="modal" class="btn btn-primary edit-item">Edit</button> ';

        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';

	  	rows = rows + '</tr>';
        i++;

	});

	$("tbody").html(rows);


    var sf='';
            sf+='<div class="col-lg-6 margin-tb">';
        sf+='<form name="staff_create_" id="staff_create_frm" class="form-horizontal" method="post" enctype="multipart/form-data">';
          sf+='<div class="form-group">';
            sf+='<label for="staff_name">Name:</label>';
            sf+='<input type="text" required name="staff_name" class="form-control" id="staff_name">';
          sf+='</div>';
          sf+='<div class="form-group">';
            sf+='<label for="contact_no">Contact Number:</label>';
            sf+='<input type="text" required class="form-control" name="contact_number" id="contact_no">';
          sf+='</div>';
          sf+='<div class="form-group">';
            sf+='<label for="contact_no">Hobby:</label>';
           var hobby='' ;
                $.each(data.hobbies, function (key, val) {
                    hobby+='<div class="checkbox">';
                    hobby+='<label><input type="checkbox" name="hobby[]" value="'+val.id+'">'+val.hobby_name+'</label>';
                    hobby+='</div>';
                });
            sf+=hobby;
            sf+='</div>';

        var category='' ;
            sf+='<div class="form-group">';
            sf+='<label for="category">Category:</label>';
            sf+='<select name="category_id" required class="form-control">';
            sf+='<option value="">==SELECT==</option>';
            $.each(data.categories, function (key, val) {
                    category+='<option value="'+val.id+'">'+val.category_name+'</option>';
                });
            sf+=category;
            sf+='</select>';
            sf+='</div>';

          
          sf+='<div class="form-group">';
            sf+='<label for="profile_pic">Profile Pic:</label>';
            sf+='<input type="file" required class="form-control" name="profile_pic" id="profile_pic_id">';
          sf+='</div>';
          
          sf+='<button type="button" class="btn btn-success submit_btn" style="margin-right:10px">Submit</button>';
          sf = sf + '<button class="btn btn-default reset_view_btn">Cancel</button>';
        sf+='</form>';
        sf+='</div>';
        $('.staff_create').html(sf);
        
}


/* Remove Post */

$("body").on("click",".remove-item",function(){

    var x = confirm("Are you sure you want to delete?");
    if (x){
        var id = $(this).parent("td").data('id');
        var c_obj = $(this).parents("tr");
         $.ajax({

            dataType: 'json',

            type:'delete',

            url: url + '/' + id,

        }).done(function(data){

            c_obj.remove();

        });
    }else{

    }
   

});

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}


/* Edit Post */

$("body").on("click",".edit-item",function(){

    var id = $(this).parent("td").data('id');
    $.ajax({
        type: 'POST',
        url: '/staff_edit',
        data: {id:id}
    }).done(function(data){
        
           var sf='';
            sf+='<div class="col-lg-6 margin-tb">';
        sf+='<form name="staff_create_" id="staff_create_frm" class="form-horizontal" method="post" enctype="multipart/form-data">';
          sf+='<div class="form-group">';
            sf+='<label for="staff_name">Name:</label>';
            sf+='<input type="text" required name="staff_name" class="form-control" id="staff_name" value="'+data.staff.staff_name+'">';
          sf+='</div>';
          sf+='<div class="form-group">';
            sf+='<label for="contact_no">Contact Number:</label>';
            sf+='<input type="text" required class="form-control" name="contact_number" id="contact_no" value="'+data.staff.contact_number+'">';
          sf+='</div>';
          sf+='<div class="form-group">';
            sf+='<label for="contact_no">Hobby:</label>';
           var hobby='' ;
                $.each(data.hobbies, function (key, val) {
                    if(isInArray(val.id, data.staff_hobby)){
                        var checkbox_stat="checked";
                    }else{
                        var checkbox_stat="";
                    }
                    hobby+='<div class="checkbox">';
                    hobby+='<label><input type="checkbox" name="hobby[]" value="'+val.id+'" '+checkbox_stat+'>'+val.hobby_name+'</label>';
                    hobby+='</div>';
                });
            sf+=hobby;
            sf+='</div>';

        var category='' ;
            sf+='<div class="form-group">';
            sf+='<label for="category">Category:</label>';
            sf+='<select name="category_id" required class="form-control">';
            sf+='<option value="">==SELECT==</option>';
            $.each(data.categories, function (key, val) {
                    if(val.id==data.staff.category_id){
                        var drop_stat="selected";
                    }else{
                        var drop_stat="";
                    }
                    category+='<option value="'+val.id+'" '+drop_stat+'>'+val.category_name+'</option>';
                });
            sf+=category;
            sf+='</select>';
            sf+='</div>';

          
          sf+='<div class="form-group">';
            sf+='<label for="profile_pic">Profile Pic:</label>';
            sf+='<input type="file" class="form-control" name="profile_pic" id="profile_pic_id">';
          sf+='</div>';
          var staff_pic="";
          if(data.staff.profile_pic != ''){
            staff_pic+='<div class="form-group">';
            staff_pic+='<img src="images/'+data.staff.profile_pic+'" height="100" width="100">';
            staff_pic+='</div>';
          }
          sf+=staff_pic;
           sf+='<input type="hidden" value="'+id+'" name="staff_id"/>';
          sf+='<button type="button" class="btn btn-success update_btn" style="margin-right:10px">Update</button>';
          sf = sf + '<button class="btn btn-default reset_view_btn">Cancel</button>';
        sf+='</form>';
        sf+='</div>';
        $('.staff_create').html(sf);
         $('.staff_record').css('display','none');
         $('.staff_create').css('display','block');
    });

});

$(".create_item").on("click", function(){
    $('.staff_record').css('display','none');
    $('.staff_create').css('display','block');
});
$("body").on("click",".reset_view_btn",function(){
    manageData();
});
$("body").on("click",".bulk_delete",function(event){
    if ($(".staff_sel_cls").is(":checked")) {
        var x = confirm("Are you sure you want to delete selected record?");
        if (x){
             event.preventDefault();
            var searchIDs = $("input:checkbox:checked").map(function(){
              return $(this).val();
            }).get(); 
            var jsonString = JSON.stringify(searchIDs);

            $.ajax({
                    url: '/bulk_delete',
                    type: 'POST',
                    data: {
                        data :jsonString
                    },
                    success: function(result)
                    {
                        manageData();
                    },
                    error: function(data)
                    {
                        console.log('error',data);
                    }
                });
        }
    }else{
        alert('please select first records!');
    }
});

