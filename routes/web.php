<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('my-staffs');
});

Route::get('my-staffs', 'StaffController@myStaffs');
Route::post('bulk_delete', 'StaffController@bulkDelete');
Route::post('staff_edit', 'StaffController@editStaff');
Route::post('staff_update', 'StaffController@updateStaff');
Route::resource('staffs','StaffController');