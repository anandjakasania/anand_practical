<!DOCTYPE html>

<html>

<head>
	<title>Anand's Practical</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
</head>

<body>

	<div class="container">
			<div class="staff_record">
				<div class="row">
				    <div class="col-lg-12 margin-tb">					
				        <div class="pull-left">
				            <h2>Anand's Practical</h2>
				        </div>
				        <div class="pull-right">
							<input type="button" class="btn btn-success create_item" value="Add New"/>
							<input type="button" class="btn btn-danger bulk_delete" value="Bulk delete"/>
				        </div>
				    </div>
				</div>

				<table class="table table-bordered">
					<thead>
					   <tr>
					    <th>Srno</th>
					    <th>Select</th>
						<th>Name</th>
						<th>Contact No</th>
						<th>Category</th>
						<th>Profile Pic</th>
						<th width="200px">Action</th>
					   </tr>
					</thead>
					<tbody>
					</tbody>

				</table>
				</div>

		<div class="staff_create" style="display:none">
		</div>
	</div>

	<script type="text/javascript">

		var url = "<?php echo route('staffs.index')?>";
		var create_url = "<?php echo route('staffs.store')?>";

	</script>
	<script src="/js/staffs-ajax.js"></script> 

</body>

</html>