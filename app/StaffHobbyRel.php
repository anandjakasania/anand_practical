<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffHobbyRel extends Model
{
    //
     public function staffHobby()
    {
        return $this->belongsTo(Staff::class);
    }
}
