<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //
     protected $fillable = [
        'staff_name', 'contact_number','profile_pic','category_id'
    ];

    public function hobbies()
    {
        return $this->hasMany(StaffHobbyRel::class);
    }

     public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
