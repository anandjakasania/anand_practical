<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\Hobby;
use App\StaffHobbyRel;
use App\Category;

class StaffController extends Controller
{
    //

     public function myStaffs()
    {
        return view('my-staffs');
       
    }

    public function index()
    {

        $staffs = Staff::with('category')->get();
        $categories = Category::all();
        $hobbies = Hobby::all();

        $main_res=array();
        $main_res['staffs']=$staffs;
        $main_res['categories']=$categories;
        $main_res['hobbies']=$hobbies;
        return response()->json($main_res);

    }
    public function bulkDelete(Request $request)
    {
        $staffs=json_decode($request->data,true);
        
        for($i=0; $i<count($staffs); $i++){
            Staff::find($staffs[$i])->delete();
        }

        return response()->json('Success');
    }

     public function store(Request $request)
    {	
    	$image = $request->file('profile_pic');
    	$filename = time().'.'.$image->getClientOriginalExtension();
    	$destinationPath = public_path('/images');
    	$image->move($destinationPath, $filename);

        $staff = new Staff;
		$staff->staff_name = $request->staff_name;
		$staff->contact_number = $request->contact_number;
		$staff->profile_pic = $filename;
		$staff->category_id = $request->category_id;
		$staff->save();

		$staff_id = $staff->id;

		$hobbies=$request->hobby;
		for($i=0; $i<count($hobbies); $i++){
			$hobby = new StaffHobbyRel;
			$hobby->staff_id=$staff_id;
			$hobby->hobby_id=$hobbies[$i];
			$hobby->save();
		}
	
        return response()->json($staff);

    }

    public function editStaff(Request $request)
    {

       $staff = Staff::with('hobbies')->where('id',$request->id)->first();
       $categories = Category::all();
       $hobbies = Hobby::all();

       $staff_hobby = array_column(json_decode($staff->hobbies,true), 'hobby_id');
        
       $main_arr=array();
       $main_arr['staff']=$staff;
       $main_arr['categories']=$categories;
       $main_arr['hobbies']=$hobbies;
       $main_arr['staff_hobby']=$staff_hobby;
        return response()->json($main_arr);
    }

     public function updateStaff(Request $request)
    {
        
        $image = $request->file('profile_pic');
        if($image){
            $filename = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $filename);
            $staff = Staff::find($request->staff_id)->update(['profile_pic' => $filename]);
        }
       
        $staff = Staff::find($request->staff_id)->update(['staff_name' =>$request->staff_name,'contact_number' =>$request->contact_number,'category_id' =>$request->category_id]);

        $del_rec=StaffHobbyRel::where('staff_id',$request->staff_id)->delete();

        $hobbies=$request->hobby;
        for($i=0; $i<count($hobbies); $i++){
            $hobby = new StaffHobbyRel;
            $hobby->staff_id=$request->staff_id;
            $hobby->hobby_id=$hobbies[$i];
            $hobby->save();
        }

        return response()->json($staff);
    }

     public function destroy($id)
    {
        Staff::find($id)->delete();
        return response()->json(['done']);
    }


}
