<?php

use Illuminate\Database\Seeder;

class HobbiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('hobbies')->insert(
        	array('hobby_name' => 'Programming'), 
			array('hobby_name' => 'Reading'), 
			array('hobby_name' => 'Games'), 
			array('hobby_name' => 'Photography')
		);
    }
}
